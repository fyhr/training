package dk.fyhr.kata.workflow.test;

import dk.fyhr.kata.workflow.*;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 00:58
 */
public class JobTest
{
    @Test
    public void addStep_GivenJobAcceptNewSteps_StepIsAdded()
    {
        Job job  = new Job();
        Step step = new Step();

        job.addStep(step);

        assertTrue(job.getTaskContainers().iterator().hasNext());
        assertEquals(step, job.getTaskContainers().iterator().next());
    }
}
