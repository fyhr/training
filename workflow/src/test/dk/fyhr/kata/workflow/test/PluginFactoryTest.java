package dk.fyhr.kata.workflow.test;

import dk.fyhr.kata.workflow.Task;
import dk.fyhr.kata.workflow.exceptions.PluginMissingException;
import dk.fyhr.kata.workflow.plugins.*;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 03:04
 */
public class PluginFactoryTest
{
    @Test
    public void create_GivenPluginAId_ReturnInstanceOfPluginA() throws PluginMissingException
    {
        PluginFactoryImpl factory = new PluginFactoryImpl();
        Task task = new Task();
        task.setPluginId("dk.fyhr.kata.workflow.plugins.PluginA, 1.0.0");

        Plugin result = factory.create(task);

        assertThat(result, instanceOf(PluginA.class));
        assertEquals(task.getPluginId(), result.getPluginId());
    }

    @Test
    public void create_GivenPluginBId_ReturnInstanceOfPluginB() throws PluginMissingException
    {
        PluginFactoryImpl factory = new PluginFactoryImpl();
        Task task = new Task();
        task.setPluginId("dk.fyhr.kata.workflow.plugins.PluginB, 1.0.0");

        Plugin result = factory.create(task);

        assertThat(result, instanceOf(PluginB.class));
        assertEquals(task.getPluginId(), result.getPluginId());
    }

    @Test(expected = PluginMissingException.class)
    public void create_GivenInvalidPluginId_ThrowException() throws PluginMissingException
    {
        PluginFactoryImpl factory = new PluginFactoryImpl();
        Task task = new Task();
        task.setPluginId("invalid pluginId");

        factory.create(task);
    }
}
