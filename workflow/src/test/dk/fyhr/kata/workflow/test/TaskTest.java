package dk.fyhr.kata.workflow.test;

import dk.fyhr.kata.workflow.Task;
import dk.fyhr.kata.workflow.exceptions.PluginMissingException;
import dk.fyhr.kata.workflow.plugins.*;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 03:02
 */
public class TaskTest
{
    @Test
    public void execute_GivenValidPluginId_InstantiateAndExecutePlugin() throws PluginMissingException
    {
        PluginFactory factoryMock = mock(PluginFactory.class);
        Plugin        plugin      = mock(Plugin.class);
        Task          task        = Task.createWithCustomPluginFactory(factoryMock);
        task.setPluginId("some pluginId");
        when(factoryMock.create(task)).thenReturn(plugin);

        task.execute();

        verify(plugin).execute();
    }
}
