package dk.fyhr.kata.workflow.test;

import dk.fyhr.kata.workflow.Step;
import dk.fyhr.kata.workflow.Task;
import dk.fyhr.kata.workflow.Transaction;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 01:14
 */
public class StepTest
{
    @Test
    public void getTransactions_GivenStepWithNoTasks_TaskIsAdded()
    {
        Step step = new Step();
        Task task = new Task();

        step.addTransaction(task);

        assertTrue(step.getTransactions().iterator().hasNext());
        assertEquals(task, step.getTransactions().iterator().next());
    }

    @Test
    public void getTransactions_GivenStepContainsOneNestedStep_ReturnAllTasks()
    {
        Step step1 = new Step();
        Task task1 = new Task();
        Step step2 = new Step();
        Task task2 = new Task();
        step1.addTaskContainer(step2);
        step1.addTransaction(task1);
        step2.addTransaction(task2);

        Iterator<Transaction> result = step1.getTransactions().iterator();

        assertEquals(task1, result.next());
        assertEquals(task2, result.next());
    }

    @Test
    public void addTaskContainer_GivenJobAcceptNewSteps_StepIsAdded()
    {
        Step step1 = new Step();
        Step step2 = new Step();

        step1.addTaskContainer(step2);

        assertTrue(step1.getTaskContainers().iterator().hasNext());
        assertEquals(step2, step1.getTaskContainers().iterator().next());
    }
}
