package dk.fyhr.kata.workflow;

import java.util.ArrayList;
import java.util.Collection;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 01:08
 */
public class Step implements TaskContainer
{
    private Collection<Transaction> _Tasks;
    private Collection<TaskContainer> _TaskContainers;

    public Step()
    {
        _Tasks = new ArrayList<>();
        _TaskContainers = new ArrayList<>();
    }

    @Override
    public void addTransaction(Transaction transaction)
    {
        _Tasks.add(transaction);
    }

    @Override
    public void addTaskContainer(TaskContainer taskContainer)
    {
        _TaskContainers.add(taskContainer);
    }

    @Override
    public Iterable<Transaction> getTransactions()
    {
        Collection<Transaction> availableTasks = new ArrayList<>();
        availableTasks.addAll(_Tasks);
        availableTasks.addAll(getNestedTransactions());

        return availableTasks;
    }

    private Collection<Transaction> getNestedTransactions()
    {
        Collection<Transaction> availableTasks = new ArrayList<>();

        TaskContainer currentContainer = getCurrentTaskContainer();

        if(currentContainer != null)
           for (Transaction transaction : currentContainer.getTransactions())
           {
               availableTasks.add(transaction);
           }

        return availableTasks;
    }

    private TaskContainer getCurrentTaskContainer()
    {
        for(TaskContainer taskContainer : _TaskContainers)
        {
            if(!taskContainer.isDone())
                return taskContainer;
        }

        return null;
    }

    @Override
    public Iterable<TaskContainer> getTaskContainers()
    {
        return _TaskContainers;
    }

    @Override
    public boolean isDone()
    {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
