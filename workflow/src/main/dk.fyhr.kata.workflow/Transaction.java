package dk.fyhr.kata.workflow;

import dk.fyhr.kata.workflow.exceptions.PluginMissingException;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 16:40
 */
public interface Transaction
{
    void execute() throws PluginMissingException;
}
