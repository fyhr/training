package dk.fyhr.kata.workflow;

import dk.fyhr.kata.workflow.exceptions.PluginMissingException;
import dk.fyhr.kata.workflow.plugins.Plugin;
import dk.fyhr.kata.workflow.plugins.PluginFactory;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 01:23
 */
public class Task implements Transaction
{
    private final PluginFactory _PluginFactory;

    private String _PluginId;

    public Task()
    {
        _PluginFactory = null;
    }

    private Task(PluginFactory pluginFactory)
    {
        _PluginFactory = pluginFactory;
    }

    public void setPluginId(String pluginId)
    {
        _PluginId = pluginId;
    }

    public String getPluginId()
    {
        return _PluginId;
    }

    public static Task createWithCustomPluginFactory(PluginFactory pluginFactory)
    {
        return new Task(pluginFactory);
    }

    public void execute() throws PluginMissingException
    {
        Plugin plugin = _PluginFactory.create(this);

        plugin.execute();
    }
}
