package dk.fyhr.kata.workflow;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 16:42
 */
public interface TaskContainer
{
    void addTaskContainer(TaskContainer taskContainer);
    Iterable<TaskContainer> getTaskContainers();

    boolean isDone();

    void addTransaction(Transaction transaction);

    Iterable<Transaction> getTransactions();
}
