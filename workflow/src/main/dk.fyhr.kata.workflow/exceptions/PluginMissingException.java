package dk.fyhr.kata.workflow.exceptions;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 16:14
 */
public class PluginMissingException extends Exception
{
    public PluginMissingException(String message){ super(message);}
}
