package dk.fyhr.kata.workflow.plugins;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 15:50
 */
public interface Plugin
{
    String getPluginId();

    void execute();
}
