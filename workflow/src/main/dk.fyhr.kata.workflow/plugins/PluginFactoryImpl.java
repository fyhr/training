package dk.fyhr.kata.workflow.plugins;

import dk.fyhr.kata.workflow.Task;
import dk.fyhr.kata.workflow.exceptions.PluginMissingException;

import java.util.HashMap;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 15:49
 */
public class PluginFactoryImpl implements PluginFactory
{
    private static HashMap<String, PluginFactory> _PluginCreationStrategies = new HashMap<>();

    static {
        _PluginCreationStrategies.put("dk.fyhr.kata.workflow.plugins.PluginA, 1.0.0", new PluginFactory()
        {
            @Override
            public Plugin create(Task task)
            {
                return new PluginA(task);
            }
        });
        _PluginCreationStrategies.put("dk.fyhr.kata.workflow.plugins.PluginB, 1.0.0", new PluginFactory()
        {
            @Override
            public Plugin create(Task task)
            {
                return new PluginB(task);
            }
        });
    }

    public Plugin create(Task task) throws PluginMissingException
    {
        if(!_PluginCreationStrategies.containsKey(task.getPluginId()))
            throw new PluginMissingException("Plugin with Id: " + task.getPluginId() + " could not be found");

        return _PluginCreationStrategies.get(task.getPluginId()).create(task);
    }
}
