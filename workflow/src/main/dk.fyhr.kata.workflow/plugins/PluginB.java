package dk.fyhr.kata.workflow.plugins;

import dk.fyhr.kata.workflow.Task;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 15:58
 */
public class PluginB implements Plugin
{
    private Task _Task;

    public PluginB(Task task)
    {
        _Task = task;
    }

    @Override
    public String getPluginId()
    {
        return _Task.getPluginId();
    }

    @Override
    public void execute()
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
