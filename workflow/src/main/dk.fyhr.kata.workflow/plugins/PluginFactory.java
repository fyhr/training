package dk.fyhr.kata.workflow.plugins;

import dk.fyhr.kata.workflow.Task;
import dk.fyhr.kata.workflow.exceptions.PluginMissingException;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 16:03
 */
public interface PluginFactory
{
    Plugin create(Task task) throws PluginMissingException;
}
