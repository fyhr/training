package dk.fyhr.kata.workflow;


import java.util.ArrayList;
import java.util.Collection;

/**
 * User: Jesper Fyhr Knudsen
 * Date: 01-11-13
 * Time: 00:58
 */
public class Job
{
    private Collection<TaskContainer> _TaskContainers;

    public Job()
    {
        _TaskContainers = new ArrayList<>();
    }

    public void addStep(TaskContainer taskContainer)
    {
        _TaskContainers.add(taskContainer);
    }

    public Iterable<TaskContainer> getTaskContainers()
    {
        return _TaskContainers;
    }
}
